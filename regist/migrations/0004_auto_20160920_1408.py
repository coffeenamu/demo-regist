# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regist', '0003_auto_20160920_1404'),
    ]

    operations = [
        migrations.RenameField(
            model_name='person',
            old_name='certpassword',
            new_name='cert_pass',
        ),
    ]
