# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regist', '0002_person_hellonumber'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='hellonumber',
        ),
        migrations.AddField(
            model_name='person',
            name='certpassword',
            field=models.CharField(max_length=100, default=0),
        ),
        migrations.AlterField(
            model_name='person',
            name='name',
            field=models.CharField(max_length=50, default=0),
        ),
    ]
